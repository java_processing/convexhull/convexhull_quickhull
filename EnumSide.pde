enum EnumSide {
  Left, Right;
  
  public static float getSin(PVector beginEdge, PVector endEdge, PVector point) {
    float algbrEdgeDist = algbrEdgeDist(beginEdge, endEdge, point);
    return algbrEdgeDist / PVector.dist(beginEdge, point);
  }
  
  public static float algbrEdgeDist(PVector beginEdge, PVector endEdge, PVector point) {
    PVector vectHull = PVector.sub(beginEdge, endEdge).normalize();
    PVector vectPrcsdPoint = PVector.sub(beginEdge, point);
    return (vectPrcsdPoint.cross(vectHull)).z;
  }

  public static EnumSide whichSide(PVector beginEdge, PVector endEdge, PVector point) {
    if (algbrEdgeDist(beginEdge, endEdge, point) > 0) {
      return EnumSide.Left;
    } else {
      return EnumSide.Right;
    }
  }
}
