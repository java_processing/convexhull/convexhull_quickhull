abstract class Step {

  protected ConvexHull convexHull;
  protected PointCloud unvalidPrcsdPnt;
  protected PointCloud pntsToProcess;
  protected int sizeToPrcs;
  protected ListIterator<PVector> convexHullIterator;
  protected ListIterator<PVector> pntsToProcessIterator;
  protected PVector beingPrcsdPoint;

  Step() {
  }
  Step(ConvexHull cnvxHull, int beginCvHlIterator) {
    this.convexHull = cnvxHull;
    this.unvalidPrcsdPnt = convexHull.getUnvalidProcessedPoints();
    this.pntsToProcess = convexHull.getPointsToProcess();
    this.sizeToPrcs = pntsToProcess.size();
    this.convexHullIterator = convexHull.listIterator(beginCvHlIterator);
    this.pntsToProcessIterator = pntsToProcess.listIterator();
  }

  public abstract boolean update();
  public void show() {
    stroke(0, 126, 255);
    strokeWeight(20);
    if (pntsToProcessIterator.hasNext()) {
      try {
        point(beingPrcsdPoint.x, beingPrcsdPoint.y);
      } 
      catch (NullPointerException e) {
      }
    }
  }
}
